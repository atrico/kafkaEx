package kafkaEx

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type TopicMap interface {
	Match(topic string) (matched bool, replacements []string)
	To(replacements []string) string
}

func NewTopicMap(text string) (tMap TopicMap, err error) {
	parts := strings.Split(text, ";")
	if len(parts) == 2 {
		tm := topicMap{to: parts[1]}
		if tm.from, err = regexp.Compile(parts[0]); err == nil {
			tMap = tm
		}
	} else {
		err = errors.New(fmt.Sprintf("invalid topic map: %s", text))
	}
	return tMap, err
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type topicMap struct {
	from *regexp.Regexp
	to   string
}

func (t topicMap) Match(topic string) (matched bool, replacements []string) {
	result := t.from.FindStringSubmatch(topic)
	return result != nil, result
}

func (t topicMap) To(replacements []string) (result string) {
	result = t.to
	for i := range replacements {
		result = strings.ReplaceAll(result, fmt.Sprintf("$%d", i), replacements[i])
	}
	return result
}
