package kafkaEx

import (
	"fmt"
	"gitlab.com/atrico/core"
	"regexp"
	"strconv"
	"strings"

	"github.com/Shopify/sarama"
)

type TopicInfo interface {
	fmt.Stringer
	Name() string
	Partition() int32
	Match(regex *regexp.Regexp) bool
	Map(topicMaps []TopicMap) TopicInfo
}

func NewTopicInfo(name string, partition int32) TopicInfo {
	return topicInfo{name, partition}
}

func NewTopicInfoFromMsg(msg *sarama.ProducerMessage) TopicInfo {
	return topicInfo{msg.Topic, msg.Partition}
}

func ParseTopicInfo(name string) TopicInfo {
	parts := strings.Split(name, ":")
	if len(parts) < 2 {
		parts = append(parts, "0")
	}
	var partition = 0
	var err error
	if partition, err = strconv.Atoi(parts[1]); err != nil {
		partition = 0
	}
	return topicInfo{parts[0], int32(partition)}
}

func FilterTopics(topics []TopicInfo, include []string, exclude []string) (result []TopicInfo) {
	includeExclude := core.NewFilter(include, exclude)
	// Filter topics
	for _, topic := range topics {
		if includeExclude.IsIncluded(topic.String()) {
			result = append(result, topic)
		}
	}
	return result
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type topicInfo struct {
	name      string
	partition int32
}

func (t topicInfo) Name() string {
	return t.name
}

func (t topicInfo) Partition() int32 {
	return t.partition
}

func (t topicInfo) String() string {
	return fmt.Sprintf("%s:%d", t.name, t.partition)
}

func (t topicInfo) Match(regex *regexp.Regexp) bool {
	return regex.Match([]byte(t.String()))
}

func (t topicInfo) Map(topicMaps []TopicMap) TopicInfo {
	from := t.String()
	for _, tm := range topicMaps {
		if matched, replacements := tm.Match(from); matched {
			return ParseTopicInfo(tm.To(replacements))
		}
	}
	return t
}
