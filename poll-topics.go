package kafkaEx

import (
	"context"
	"github.com/Shopify/sarama"
	"github.com/rs/zerolog/log"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/core/collection/set"
	"time"
)

func GetTopics(consumer sarama.Consumer) (topics []TopicInfo, err error) {
	return GetTopicsFiltered(consumer, core.AcceptAllFilter)
}

func GetTopicsFiltered(consumer sarama.Consumer, filter core.StringFilter) (topics []TopicInfo, err error) {
	var topicNames []string
	if topicNames, err = consumer.Topics(); err == nil {
		for _, topicName := range topicNames {
			var partitions []int32
			if partitions, err = consumer.Partitions(topicName); err == nil {
				for _, partition := range partitions {
					topicInf := NewTopicInfo(topicName, partition)
					if filter.IsIncluded(topicInf.String()) {
						topics = append(topics, topicInf)
					}
				}
			}
		}
	}
	return
}

func PollForNewTopics(ctx context.Context, client sarama.Client, consumer sarama.Consumer, topicFilter core.StringFilter, delay time.Duration) <-chan TopicInfo {
	ch := make(chan TopicInfo, 0)
	go func() {
		defer close(ch)
		oldTopics := set.MakeMutableSet[string]()
		more := ctx.Err() == nil
		var err error
		var topicNames []string
		for more {
			if err = client.RefreshMetadata(); err == nil {
				if topicNames, err = consumer.Topics(); err == nil {
					for _, topicName := range topicNames {
						if !oldTopics.Contains(topicName) {
							oldTopics.Add(topicName)
							if partitions, err2 := consumer.Partitions(topicName); err2 == nil {
								for _, partition := range partitions {
									topicInf := NewTopicInfo(topicName, partition)
									if topicFilter.IsIncluded(topicInf.String()) {
										ch <- topicInf
									}
								}
							} else {
								log.Warn().Err(err2).Msgf("error reading topic partitions for %s", topicName)
							}
						}
					}
				} else {
					log.Error().Err(err).Msg("error reading topics")
					more = false
				}
			} else {
				log.Error().Err(err).Msg("error refreshing metadata")
				more = false
			}
			// Delay
			select {
			case <-ctx.Done():
				log.Trace().Msg("Topic poll cancelled")
				more = false
			case <-time.After(delay):
				// Loop again
			}
		}
	}()
	return ch
}
