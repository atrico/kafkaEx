package kafkaEx

import (
	"github.com/Shopify/sarama"
	"strconv"
	"strings"
)

// Accept string oldest, newest or numeric offset
func ParseOffset(str string) (offset int64, err error) {
	if strings.HasPrefix(strings.ToLower(str), "old") {
		offset = sarama.OffsetOldest
	} else if strings.HasPrefix(strings.ToLower(str), "new") {
		offset = sarama.OffsetNewest
	} else {
		offset, err = strconv.ParseInt(str, 10, 64)
	}
	return offset, err
}
